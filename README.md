# Budget

## Description 
This project aims to create a simple application in Electron to manage a budget and to run different kind of tests on it as unit tests, end-to-end tests and integration tests.

## How running the project
To run the project, you need to have Node.js installed on your machine. Execute these commands : 
```
npm install
npm run start
```

## How running the tests
To run the tests : 
```
npm run test
```